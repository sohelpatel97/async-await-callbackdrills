const fs = require('fs');


const FileCreation = () => {

    return new Promise((resolve, reject) => {

        Path = './jsonFolder'

        fs.mkdir(Path, (err) => {
            if (err) {
                console.log(err);
            }

        })

        let NumberOfFiles = 5

        let count = 0

        files = []

        for (let i = 1; i < NumberOfFiles; i++) {

            let fileName = Path + "/fileNo" + i;

            fs.writeFile(fileName, "json data", 'utf-8', (err) => {

                console.log('file created  ', fileName);

                if (err) {

                    reject(err);
                }

                count += 1

                files.push(fileName)

                if (count == NumberOfFiles - 1) {

                    resolve(files)
                }

            }); //end of write

        }


    })

}

const FileDeletion = (files) => {

    let count = 0;

    return new Promise((res, rej) => {

        files.forEach((current_file) => {

            fs.unlink(current_file, (err) => {

                count += 1

                console.log('file deleted is : ', current_file);

                if (err) {

                    rej(err);

                }
                if (count == files.length) {

                    res("All files has been Deleted!")
                }

            }); //unlike end

        }); //foreach end

    }); //return promise end
}

module.exports = { FileCreation, FileDeletion }