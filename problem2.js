const fs = require('fs')


const ReadAndUpperCaseMaker = () => {

    return new Promise((resolve, reject) => {

        fs.readFile('./lipsum.txt', "utf-8", (err, data) => {

            data = data.toUpperCase()

            err ? reject(err) : resolve(data)

        });
    })
}

const UpperCaserWriiter = (data) => {

    file1 = './uppercasefile.txt'

    return new Promise((resolve, reject) => {

        fs.writeFile(file1, data, 'utf8', function (err) {

            err ? reject(err) : resolve(file1)
        })

    })

}


const AppendFiles = (fileName) => {

    return new Promise((resolve, reject) => {

        fs.appendFile('fileNames.txt', fileName + " ", function (err) {

            err ? reject(err) : resolve("File append Operation executed")

        });

    })

}


const ReadUpperCaseSplitIntoSentences = (uppercaseFile) => {

    return new Promise((resolve, reject) => {
        fs.readFile(uppercaseFile, "utf-8", (err, data) => {

            data = data.toLowerCase().split('. ')

            err ? reject(err) : resolve(data);

            console.log(data);



        });
    })
}


const WriteSentencedData = (sentencsFile) => {

    return new Promise((resolve, reject) => {

        sentencsFile.forEach(currdata => {

            fs.appendFile("sentencedData.txt", currdata + "\n", (err) => {

                err ? reject(err) : resolve("sentencedData.txt")

            });

        });

    })
}

const ReadSentencedData = (sentencedFileName) => {

    return new Promise((resolve, reject) => {

        fs.readFile(sentencedFileName, "utf-8", (err, data) => {

           

            let ArrayOfWords = data.split('\n');
           // console.log(ArrayOfWords);
            

            var sorted = ArrayOfWords.sort();
            let result = sorted.join('\n').trim();

            err ? reject(err) : resolve(result)
        })
    })

}


const WriteSortedFile = (result) => {

    let sortedFile = './sortedData.txt';

    return new Promise((resolve, reject) => {

        fs.writeFile(sortedFile, result, 'utf8', function (err) {

            err ? reject(err) : resolve(sortedFile)

        })
    })
}


const ReadAllFiles = (filenames) => {

    return new Promise((resolve, reject) => {

        fs.readFile(filenames, "utf-8", (err, data) => {

            let FilenamesArray = data.replace(/\s+/g, ' ').split(' ');

            err ? reject(err) : resolve(FilenamesArray)
        })
    })
}


const DeleteAllFiles = (FilenamesArray) => {

    return new Promise((resolve, reject) => {

        let count = 0

        FilenamesArray.forEach((filename) => {



            if (filename != '') {

                fs.unlink(filename, (err) => {

                    console.log(filename);
                    
                    count += 1

                    if (err) {

                        reject(err)

                    } else {

                        if (count == FilenamesArray.length) {

                            resolve("Files Are Rest In Peace")
                        }
                    }
                })
            }

        });
    })
}


module.exports = { AppendFiles, ReadAndUpperCaseMaker, UpperCaserWriiter, ReadUpperCaseSplitIntoSentences, WriteSentencedData, ReadSentencedData, WriteSortedFile, ReadAllFiles, DeleteAllFiles }