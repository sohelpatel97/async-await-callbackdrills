const problem2 = require('../problem2')

const CreaterAndRemoveFiles2 = async () => {

    try {

        let UppercaseData = await problem2.ReadAndUpperCaseMaker()

        let file1 = await problem2.UpperCaserWriiter(UppercaseData)

        await problem2.AppendFiles(file1)

        let data2 = await problem2.ReadUpperCaseSplitIntoSentences(file1)

        let file2 = await problem2.WriteSentencedData(data2)

        await problem2.AppendFiles(file2)

        let data3 = await problem2.ReadSentencedData(file2)

        let file3 = await problem2.WriteSortedFile(data3)

        await problem2.AppendFiles(file3);

        let FilenamesArray = await problem2.ReadAllFiles("fileNames.txt")

        let msg = await problem2.DeleteAllFiles(FilenamesArray)

        console.log(msg)


    }
    catch (er) {


        console.log("Operation Has failed !", er);
    }
}


CreaterAndRemoveFiles2()